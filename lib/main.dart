import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    //Text Style = tittle
    final tittleTextStyle = TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.bold,
      fontFamily: 'Roboto',
      letterSpacing: 0.5,
      fontSize: 18,
    );

    //profil circle
    final profile = Container(
      width: 45,
      height: 45,
      decoration: new BoxDecoration(
        color: Colors.grey,
        shape: BoxShape.circle,
      ),
    );

    //profil circle with checklist
    final profileCheck = Stack(
      children: [
        profile,
        new Positioned(
          child: Icon(Icons.check_circle, color: Colors.blue, size: 20,),
          right: 2.5,
          bottom: 6,
        )
      ],
    );

    //notification circle
    final circleNotification = Container(
      width: 20,
      height: 20,
      decoration: new BoxDecoration(
        color: Colors.green,
        shape: BoxShape.circle,
      ),
    );

    //notification final
    final notification = Stack(
      children: [
        circleNotification,
        new Positioned(
          child: Text('1', style: TextStyle(color: Colors.white),),
          top: 2.5,
          left: 6,
        )
      ],
    );

    //isi message
    final messageContent = Row(
      children: [
        Icon(Icons.check_circle, color: Colors.black38, size: 12,),
        Text('Icinisan supriyanto',
          style: TextStyle(
            color: Colors.black45,
          ),
          textAlign: TextAlign.left,
        ),
      ],
    );

    //Column of name and desc on the content list
    final nameContent = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Supriyanto', style: tittleTextStyle, textAlign: TextAlign.left),
        messageContent
      ],
    );

    //Column of notification
    final rightContent = Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text('a second ago', style: TextStyle(
          color: Colors.black26),
          textAlign: TextAlign.right,
        ),
        notification
      ],
    );

    //Isi list
    final listContent = Container(
      padding: EdgeInsets.all(5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 15,
            child: profile
          ),
          Expanded(
              flex: 60,
              child: nameContent
          ),Expanded(
              flex: 25,
              child: rightContent
          )
        ],
      ),
    );

    //isi list dengan profil dicheck
    final listContentChecked = Container(
      padding: EdgeInsets.all(5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
              flex: 15,
              child: profileCheck
          ),
          Expanded(
              flex: 60,
              child: nameContent
          ),Expanded(
              flex: 25,
              child: rightContent
          )
        ],
      ),
    );

    //devider
    final divid = Divider(color: Colors.black45,);

    return MaterialApp(
      title: 'Message Layout',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Flutter Message Layout'),
        ),
        body: Center(
          child:Container(
            padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text('Messages', style: tittleTextStyle, textAlign: TextAlign.left),
              divid,
              listContentChecked,
              divid,
              listContentChecked,
              divid,
              listContent,
              divid,
              listContent,
              divid,
              Text('Lihat Semua', style: TextStyle(color: Colors.blue), textAlign: TextAlign.center)
            ],
          ),
          ),
        ),
      ),
    );
  }
}
